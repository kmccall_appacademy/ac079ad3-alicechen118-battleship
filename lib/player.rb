class HumanPlayer

  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def get_play
    puts "Please enter your target (ex. 9, 4): "
    gets.chomp.split(", ").map { |val| Integer(val) }
  end

end
