class BattleshipGame

  attr_reader :board, :player
  attr_accessor :ship_hit

  def initialize(player=HumanPlayer.new("Alice"), board=Board.random)
    @player = player
    @board = board
    @ship_hit = false
  end

  def play
   play_turn until game_over?
   puts "You sunk all of your opponent's ships!"
  end

  def attack(pos)
    if board[pos] == :s
      @ship_hit = true
    else
      @ship_hit = false
    end
    board[pos] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    pos = nil
    until valid_turn?(pos)
      board.display
      puts "Opponent's ship hit!" if ship_hit
      puts "Your opponent has #{count} intact ships remaining."
      pos = player.get_play
    end
    attack(pos)
  end

  def valid_turn?(pos)
    pos.is_a?(Array) && pos.all? { |pos| (0...board.grid.length).include?(pos) }
  end

end

if __FILE__ == $PROGRAM_NAME
  BattleshipGame.new.play
end
