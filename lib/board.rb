class Board

  DISPLAY_HASH = {
    nil => " ",
    :s => " ",
    :x => "x"
  }

  attr_reader :grid
  attr_accessor :marks

  def initialize(grid=Board.default_grid)
    @grid = grid
    @marks = [:s, :x]
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def random
    self.new(self.default_grid, true)
  end

  def display
  header = (0..9).to_a.join("  ")
  p "  #{header}"
  grid.each_with_index { |row, i| display_row(row, i) }
  end

  def display_row(row, i)
  chars = row.map { |val| DISPLAY_HASH[val] }.join("  ")
  p "#{i} #{chars}"
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    grid[row][col] = val
  end

  def count
    ships = grid.flatten.select { |val| val == :s }
    ships.length
  end

  def empty?(pos=nil)
    if pos
      grid[pos[0]][pos[1]] == nil
    else
      return true if count == 0
      return false if count > 0
    end
  end

  def full?
    grid.flatten.all? { |val| @marks.include?(val) }
  end

  def place_random_ship
    raise "Invalid move, board is full." if full?
    pos = random_pos
    grid[pos[0]][pos[1]] = :s if empty?(pos)
  end

  def random_pos
    [rand(0...grid.length), rand(0...grid.length)]
  end

  def won?
    grid.flatten.none? { |val| val == :s }
  end

end
